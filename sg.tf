#Liberando acesso ssh
resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow ssh access"

  ingress {
    description       = "ssh from vpc"
    from_port         = "22"
    to_port           = "22"
    protocol          = "tcp"
    cidr_blocks       = ["0.0.0.0/0"]

}

  tags = {
    Name = "allow_ssh"
  }
}
#Liberando acesso http
resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow http access port 80"

  ingress {
    description       = "http from vpc"
    from_port         = "80"
    to_port           = "80"
    protocol          = "tcp"
    cidr_blocks       = ["0.0.0.0/0"]

}

  tags = {
    Name = "allow_http"
  }
}

#Liberando a partir da vm para fora (egress)
resource "aws_security_group" "allow_egress" {
  name        = "allow_egress"
  description = "Allow egress"

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1" #libera todos os protocolos
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_egress"
  }
}