resource "aws_instance" "gitlab-website-1" {
  ami           = "ami-080e1f13689e07408"
  instance_type = "t2.micro"
  key_name = "terraform" #key access
  security_groups = ["allow_ssh","allow_http","allow_egress"] #vinculando os sg´s
  #user_data > especificar qual será o arquivo de script
  user_data = file("script.sh")


  tags = {
    Name = "gitlab-website-1"
  }
}